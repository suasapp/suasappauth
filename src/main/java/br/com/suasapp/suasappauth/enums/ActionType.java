package br.com.suasapp.suasappauth.enums;

public enum ActionType {

    CREATE("CREATE"),
    READ("READ"),
    UPDATE("UPDATE"),
    DELETE("DELETE");

    private final String action;

    ActionType(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return action;
    }
}
