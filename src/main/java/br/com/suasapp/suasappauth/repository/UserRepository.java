package br.com.suasapp.suasappauth.repository;

import br.com.suasapp.suasappauth.model.User;
import br.com.suasapp.suasappgenerics.repository.AbstractRepository;

public interface UserRepository extends AbstractRepository<User> {

    User findUserByUsernameAndActiveTrue(String username);

}
