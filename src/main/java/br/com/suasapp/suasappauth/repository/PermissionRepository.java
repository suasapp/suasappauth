package br.com.suasapp.suasappauth.repository;

import br.com.suasapp.suasappauth.model.Permission;
import br.com.suasapp.suasappgenerics.repository.AbstractRepository;

public interface PermissionRepository extends AbstractRepository<Permission> {
}
