package br.com.suasapp.suasappauth.repository;

import br.com.suasapp.suasappauth.model.Role;
import br.com.suasapp.suasappgenerics.repository.AbstractRepository;

public interface RoleRepository extends AbstractRepository<Role> {
}
