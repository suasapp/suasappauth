package br.com.suasapp.suasappauth.controller;

import br.com.suasapp.suasappauth.model.Permission;
import br.com.suasapp.suasappauth.service.PermissionService;
import br.com.suasapp.suasappgenerics.controller.AbstractController;
import br.com.suasapp.suasappgenerics.service.AbstractService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/permissions")
public class PermissionController extends AbstractController<Permission> {

    public PermissionController(PermissionService permissionService) {
        super(permissionService);
    }
}
