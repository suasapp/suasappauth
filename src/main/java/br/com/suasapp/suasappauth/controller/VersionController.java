package br.com.suasapp.suasappauth.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/version")
public class VersionController {

    @Value("${build.version}")
    private String version;

    @GetMapping("/")
    public ResponseEntity getVersion() {
        return ResponseEntity.ok(version);
    }

}
