package br.com.suasapp.suasappauth.controller;

import br.com.suasapp.suasappauth.model.User;
import br.com.suasapp.suasappauth.service.UserService;
import br.com.suasapp.suasappgenerics.controller.AbstractController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController extends AbstractController<User> {

    private final UserService userService;

    public UserController(UserService service) {
        super(service);
        this.userService = service;
    }

    @GetMapping("/username/{username}")
    public ResponseEntity getUserByUsername(@PathVariable("username") String username) {
        return ResponseEntity.ok(userService.findUserByUsername(username));
    }
}
