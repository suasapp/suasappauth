package br.com.suasapp.suasappauth.controller;

import br.com.suasapp.suasappauth.model.Role;
import br.com.suasapp.suasappauth.service.RoleService;
import br.com.suasapp.suasappgenerics.controller.AbstractController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
public class RoleController extends AbstractController<Role> {

    public RoleController(RoleService roleService) {
        super(roleService);
    }
}
