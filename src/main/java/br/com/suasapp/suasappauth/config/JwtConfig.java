package br.com.suasapp.suasappauth.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.com.suasapp.jwtlib.properties")
public class JwtConfig {
}
