package br.com.suasapp.suasappauth.model;

import br.com.suasapp.suasappauth.enums.ActionType;
import br.com.suasapp.suasappgenerics.model.AbstractModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "permission")
public class Permission implements AbstractModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String page;

    @Enumerated(EnumType.STRING)
    private ActionType action;

}
