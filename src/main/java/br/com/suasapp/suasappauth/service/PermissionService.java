package br.com.suasapp.suasappauth.service;

import br.com.suasapp.suasappauth.model.Permission;
import br.com.suasapp.suasappgenerics.service.AbstractService;

public interface PermissionService extends AbstractService<Permission> {
}
