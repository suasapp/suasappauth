package br.com.suasapp.suasappauth.service.impl;

import br.com.suasapp.suasappauth.model.Permission;
import br.com.suasapp.suasappauth.repository.PermissionRepository;
import br.com.suasapp.suasappauth.service.PermissionService;
import br.com.suasapp.suasappgenerics.service.AbstractServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl extends AbstractServiceImpl<Permission> implements PermissionService {

    public PermissionServiceImpl(PermissionRepository permissionRepository) {
        super(permissionRepository);
    }
}
