package br.com.suasapp.suasappauth.service;

import br.com.suasapp.suasappauth.model.User;
import br.com.suasapp.suasappgenerics.service.AbstractService;

public interface UserService extends AbstractService<User> {

    User findUserByUsername(String username);

}
