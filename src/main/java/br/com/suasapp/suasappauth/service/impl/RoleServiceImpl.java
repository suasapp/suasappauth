package br.com.suasapp.suasappauth.service.impl;

import br.com.suasapp.suasappauth.model.Role;
import br.com.suasapp.suasappauth.repository.RoleRepository;
import br.com.suasapp.suasappauth.service.RoleService;
import br.com.suasapp.suasappgenerics.service.AbstractServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends AbstractServiceImpl<Role> implements RoleService {

    public RoleServiceImpl(RoleRepository roleRepository) {
        super(roleRepository);
    }
}
