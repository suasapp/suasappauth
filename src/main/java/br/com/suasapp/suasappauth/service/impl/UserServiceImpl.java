package br.com.suasapp.suasappauth.service.impl;

import br.com.suasapp.suasappauth.model.User;
import br.com.suasapp.suasappauth.repository.UserRepository;
import br.com.suasapp.suasappauth.service.UserService;
import br.com.suasapp.suasappgenerics.service.AbstractServiceImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends AbstractServiceImpl<User> implements UserService {

    private final BCryptPasswordEncoder encoder;

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder encoder) {
        super(userRepository);
        this.encoder = encoder;
        this.userRepository = (UserRepository) repository;
    }

    public User findUserByUsername(String username) {
        return userRepository.findUserByUsernameAndActiveTrue(username);
    }

    @Override
    public User save(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        user.setActive(true);
        return super.save(user);
    }
}
