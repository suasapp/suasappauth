package br.com.suasapp.suasappauth.service;

import br.com.suasapp.suasappauth.model.Role;
import br.com.suasapp.suasappgenerics.service.AbstractService;

public interface RoleService extends AbstractService<Role> {
}
